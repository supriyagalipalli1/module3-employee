import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent implements OnInit {
  
  products: any;
  cartProducts: any;
  constructor(private service: EmpService) {

    this.cartProducts = [];
    this.products = [
      {id:1001, name:"H&M",   price:14999.00, description:"No Cost EMI Applicable", imgsrc:"assets/product1.jpg"},
      {id:1002, name:"Crape", price:24999.00, description:"No Cost EMI Applicable", imgsrc:"assets/product2.jpeg"},
      {id:1003, name:"Kurtha",  price:34999.00, description:"No Cost EMI Applicable", imgsrc:"assets/product3.jpeg"}
      
    ];
  }

  ngOnInit() {
  }
  addToCart(product: any) {
    // this.cartProducts.push(product);
    // localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));

    //Cart using Service
    this.service.addToCart(product);
  }
}

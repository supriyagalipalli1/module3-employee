import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLoggedIn: boolean;

  //Cart using Service
  cartItems: any;

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) { 
    this.isUserLoggedIn = false;
    //Cart using Service
    this.cartItems = [];
  }
  getAllCountries(){
    return this.http.get('https://restcountries.com/v3.1/all')
  }

  getEmployees(): any {
    return this.http.get('http://localhost:8080/getEmployees');
  }
  getEmployeeById(empId: any): any {
    return this.http.get('http://localhost:8080/getEmployeeById/' + empId);
  }

  //Cart using Service
  addToCart(product: any) {
    this.cartItems.push(product);
  }
  //Cart using Service
  getCartItems(): any {
    return this.cartItems;
  }

  //Login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }

  //Logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
  }

}


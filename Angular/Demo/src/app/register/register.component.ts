import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  countries: any;

  constructor(private service: EmpService,private toastr: ToastrService) {
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => {
      this.countries = data;
      console.log(data);
    });
  }
 

  registerSubmit(regForm: any) {
    console.log(regForm);

    this.toastr.success('Registration Successful!', 'Success');
  }

}
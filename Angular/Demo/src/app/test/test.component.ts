import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
    
  id : number;
  name : string;
  avg : number;
  address : any;
  hobbies : any;

  constructor() {
    alert("constructor is invoked.....");

    this.id = 101;
    this.name = 'Supriya';
    this.avg = 45.56;
    
    
    this.address = {
      streetNo : 101,
      city : 'Mancherial',
      state : 'Telangana'
    };

    this.hobbies =['Cooking', 'Sleeping'];


  }

  ngOnInit() {
    alert("ngOnInit is invoked.....");
    
  }

}

package day01;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class demo2 {

public static void main(String[] args) {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from employee";
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");
			
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			
			rs.next();
			System.out.println("empId   : " + rs.getInt(1));
			System.out.println("emp_name: " + rs.getString("emp_name"));
			System.out.println("salary  : " + rs.getDouble(3));
			System.out.println("gender  : " + rs.getString("gender"));
			System.out.println("email   : " + rs.getString(5));
			System.out.println("password: " + rs.getString(6) + "\n");
			
			rs.next();
			System.out.println("empId   : " + rs.getInt(1));
			System.out.println("emp_Name: " + rs.getString("emp_name"));
			System.out.println("salary  : " + rs.getDouble(3));
			System.out.println("gender  : " + rs.getString("gender"));
			System.out.println("email   : " + rs.getString(5));
			System.out.println("password: " + rs.getString(6) + "\n");
			
			
			rs.next();
			System.out.println("empId   : " + rs.getInt(1));
			System.out.println("emp_name: " + rs.getString("emp_name"));
			System.out.println("salary  : " + rs.getDouble(3));
			System.out.println("gender  : " + rs.getString("gender"));
			System.out.println("email   : " + rs.getString(5));
			System.out.println("password: " + rs.getString(6) + "\n");
			
			
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		
	}
}

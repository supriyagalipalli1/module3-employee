package day02;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class empId {

	public static void main(String[] args) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		System.out.print("Enter Employee Id: ");
		int empId = new Scanner(System.in).nextInt();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from employee where empId = " + empId;
		
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				System.out.println("EmpId   : " + rs.getInt(1));
				System.out.println("Emp_name : " + rs.getString("emp_name"));
				System.out.println("Salary  : " + rs.getDouble(3));
				System.out.println("Gender  : " + rs.getString("gender"));
				System.out.println("EmailId : " + rs.getString(5));
				System.out.println("Password: " + rs.getString(6) + "\n");
			} else {
				System.out.println("Employee Record Not Found!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	

}

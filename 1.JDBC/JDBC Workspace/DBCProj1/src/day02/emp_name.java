package day02;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
public class emp_name {

	public static void main(String[] args) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		System.out.print("Enter Employee Name: ");
		String emp_name = new Scanner(System.in).next();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from employee where emp_name = '" + emp_name + "'";
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				do {
					System.out.println("EmpId   : " + rs.getInt(1));
					System.out.println("Emp_name : " + rs.getString("emp_name"));
					System.out.println("Salary  : " + rs.getDouble(3));
					System.out.println("Gender  : " + rs.getString("gender"));
					System.out.println("EmailId : " + rs.getString(5));
					System.out.println("Password: " + rs.getString(6) + "\n");
				} while (rs.next());
			} else {
				System.out.println("Employee Record(s) Not Found!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

}

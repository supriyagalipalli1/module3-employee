package Assignment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class emp_name {
	public static void main(String[] args) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		System.out.print("Enter the Employee Name: ");
		String emp_name = new Scanner(System.in).next();
		System.out.println();

		String selectQuery = "select * from employee where emp_name=(?)";

		try {

			pst = con.prepareStatement(selectQuery);
			pst.setString(1, emp_name);
			rs = pst.executeQuery();

			while (rs.next()) {
				System.out.print(rs.getInt(1) + " " + rs.getString(2) + " ");
				System.out.print(rs.getDouble(3) + " " + rs.getString(4) + " ");
				System.out.println(rs.getString(5) + " " + rs.getString(6));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

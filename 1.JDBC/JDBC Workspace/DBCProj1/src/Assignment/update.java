package Assignment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class update {
	public static void main(String[] args) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;

		System.out.println("Enter Employee Id and New Salary");
		Scanner scan = new Scanner(System.in);
		int empId = scan.nextInt();
		;
		double salary = scan.nextDouble();
		System.out.println();

		String updateQuery = "update employee set salary = (?) where empId = (?) ";

		try {

			pst = con.prepareStatement(updateQuery);
			pst.setInt(2, empId);
			pst.setDouble(1, salary);
			int result = pst.executeUpdate();

			if (result > 0) {
				System.out.println("Employee Record Updated!!!");
			} else {
				System.out.println("Failed to Update the Employee Record!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {

					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

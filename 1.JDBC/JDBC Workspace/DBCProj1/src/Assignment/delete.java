package Assignment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class delete {
	public static void main(String[] args) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;

		System.out.print("Enter Employee Id: ");
		int empId = new Scanner(System.in).nextInt();
		System.out.println();

		String deleteQuery = "delete from employee where empId = (?)";

		try {

			pst = con.prepareStatement(deleteQuery);
			pst.setInt(1, empId);

			int result = pst.executeUpdate();

			if (result > 0) {
				System.out.println("Employee Record Deleted!!!");
			} else {
				System.out.println("Failed to delete the Employee Record!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {

					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
